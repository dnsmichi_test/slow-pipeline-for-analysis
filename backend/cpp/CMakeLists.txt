cmake_minimum_required(VERSION 3.1...3.26)

project(
  BackendLegacyServiceInefficient
  VERSION 1.0
  LANGUAGES CXX)

add_executable(BackendLegacyServiceInefficient main.cpp)  