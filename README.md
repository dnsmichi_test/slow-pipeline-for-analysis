# Slow Pipeline for Analysis

Showcases Observability for efficient DevSecOps Pipelines.

Talk slides: https://go.gitlab.com/VDAvMw (Cloudland 2023)


## Tools

### OpenTelemetry

### NewRelic 

### CI Visibility

Enabled in `Settings > Integrations`

## Development

### NodeJS

CLI to generate the package.json

```
npm init

npm install react react-dom webpack webpack-dev-server webpack-cli  babel-core babel-loader babel-preset-env babel-preset-react babel-webpack-plugin --save
```
