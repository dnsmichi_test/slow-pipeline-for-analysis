#!/bin/bash

PLATFORM="ARM"

function build() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Building on $PLATFORM platform"

build

echo "Finished initializing the compiler config"

build

echo "Build successful. Running integrety checks"

build