#!/bin/bash

# Simulate a download of many resources.
for MB_COUNT in 1 5 10 20
do
  dd if=/dev/urandom of=`$MB_COUNT`.txt bs=1048576 count=$MB_COUNT
done

# TODO: Upload artifacts to package registry to avoid job artifacts growing fast.

