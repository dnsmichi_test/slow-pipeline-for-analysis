#!/bin/bash

echo "Using Go version $(go version)"

echo "Installing go-junit-report"

# TODO: This wastes resources on every job run. Consider fixing.
go install github.com/jstemmer/go-junit-report/v2@latest

echo "Running tests"

sleep 10
