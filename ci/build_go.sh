#!/bin/bash

LANG="GO"

function build() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}


echo "Using Go version $(go version)"

echo "Compiling $LANG"

build

echo "Linking objects"

build

echo "Burned lots of CPU cycles, blocking all resources."

build