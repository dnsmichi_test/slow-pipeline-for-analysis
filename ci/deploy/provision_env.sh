#!/bin/bash

function prepare() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Provisioning new deployment resource"
prepare

echo "Running Terraform and Ansible"
prepare

echo "Deployment ready."
