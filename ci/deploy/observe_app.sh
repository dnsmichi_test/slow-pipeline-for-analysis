#!/bin/bash

function prepare() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Deploying Prometheus scrape target ServiceMonitor"
prepare

echo "Sleeping for 30s"
sleep 30

echo "Collecting metrics"
prepare

echo "Generating graphs for merge request update"
prepare

echo "Done"
