#!/bin/bash

function prepare() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Deploying application"
prepare

echo "Building container image"
prepare

echo "Pushing to cloud registry"
prepare

echo "Sleeping until ready"
prepare

echo "Executing remote container pull"
prepare

echo "Restarting container deployment"
prepare

echo "Done"
