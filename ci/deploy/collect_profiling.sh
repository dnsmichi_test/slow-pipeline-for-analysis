#!/bin/bash

function prepare() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Collecting profiling data"
prepare

echo "Initiating longer run, storing perf profile locally"
sleep 60

echo "Uploading perf profile to cloud"
prepare

echo "Updating MR comment with URL"
prepare

echo "Done"
