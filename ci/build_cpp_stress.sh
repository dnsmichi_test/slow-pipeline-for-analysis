#!/bin/bash

apt update && apt -y install sysbench

time sysbench cpu --threads=2 run
