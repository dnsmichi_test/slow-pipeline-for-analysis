#!/bin/bash

LANG="CPP"

function build() 
{
    RANDOM=$$
    S=$(($RANDOM%10))
    sleep $S
}

echo "Compiling $LANG"

cd backend/cpp 

mkdir debug
cd debug 
cmake ..
cd ..
make -C debug 

build

echo "Linking objects"

build

echo "Burned lots of CPU cycles, blocking all resources."

build